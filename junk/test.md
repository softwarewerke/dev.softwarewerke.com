# Eurodentplus - Changelog

## v1.0.7 2021-01-16
 
Cleanup git project


v1.0.6 2020-11-26

### Fixed
* Model347 wrong total


---

## v1.0.5 2020-11-06

### Added
* Modelo347 trimestral

---

## v1.0.4 2020-??-??

### Added
* Endpoint secured with roles

---

## v1.0.3 2020-07-30

### dded
* Recall Job

---

## v1.0.2 2020-??-??

### Changed
* Generar JAR sin version edplus.jar
* Cambiar visualizar ID pacient por Codigo paciente
* Combo paciente buscar por valor numerico si busca por cifras
* Modelo347 eliminar columna "Código" porque no es el código, si no el ID.

### Fixed
* Modelo 347 genera HTML encoding UTF8

---

## v1.0.1 2020-??-??

### Changed
* clean unsigned PDF folder remove only contents, not the folder

---

## v1.0.0 2020-03-07 initial version

### Added
### Changed
### Deprecated
### Removed
### Fixed
### Security